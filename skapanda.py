import pandas as pd
from sklearn import svm
from sklearn.linear_model import LogisticRegression

heart = pd.read_csv('SAHeart.csv', sep=',', header=0)
# print(heart.head())

y = heart.iloc[:,9]
X = heart.iloc[:,:9]

# print(y)
# print(X)

heart = heart.drop(['famhist'], axis=1)
# print(heart.head())

# y = heart.iloc[:,8]
# X = heart.iloc[:,:8]

# print(f'y = \n{y}')
# print(f'X = \n{X}')

# LR = LogisticRegression(random_state=0, solver='newton-cg', multi_class='ovr').fit(X, y)
# Затем мы можем использовать метод predict для предсказания вероятностей новых данных
# print(f'predict = {LR.predict(X.iloc[460:,:])}')
# print(f'predict = {LR.predict(X.iloc[461:,:])}')
# print(f'predict = {LR.predict(X.iloc[459:,:])}')
# print(f'predict = {LR.predict(X.iloc[458:,:])}')

# метод score для получения средней точности предсказания
# Оценка объясненной дисперсии (variance score): 1 означает идеальное предсказание
# и 0 означает, что между X и y нет линейной связи.
# print(f'score = {round(LR.score(X,y), 4)}')


# SVM = svm.LinearSVC()
# SVM.fit(X, y)
# print(SVM.predict(X.iloc[460:,:]))
# print(round(SVM.score(X,y), 4))

# heart.info()
print(heart.describe())

import matplotlib.pyplot as plt
heart.hist(bins=50, figsize=(15,8))
plt.show()