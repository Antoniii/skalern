# https://github.com/ageron/handson-ml/blob/master/02_end_to_end_machine_learning_project.ipynb

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from sklearn.model_selection import train_test_split
from pandas.plotting import scatter_matrix
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import (OrdinalEncoder,
 								   StandardScaler,
 								   FunctionTransformer,
 								   OneHotEncoder)
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline


housing = pd.read_csv('housing.csv', sep=',', header=0)
# print(housing.head())

# housing.info()

# print(housing.describe())


# housing.hist(bins=50, figsize=(15, 7))
# plt.show()

train_set, test_set = train_test_split(housing, test_size=0.2, random_state=42)
# print(test_set.head())

# housing["median_income"].hist()
# plt.title('median_income')
# plt.show()

# housing["income_cat"] = pd.cut(housing["median_income"],
#                                bins=[0., 1.5, 3.0, 4.5, 6., np.inf],
#                                labels=[1, 2, 3, 4, 5])

# print(housing["income_cat"].value_counts())
# housing["income_cat"].hist()
# plt.title('median_income_in_strate')
# plt.show()

# housing.plot(kind="scatter", x="longitude", y="latitude")
# plt.title("bad_visualization_plot")

# housing.plot(kind="scatter", x="longitude", y="latitude", alpha=0.1)
# plt.title("better_visualization_plot")


# housing.plot(kind="scatter", x="longitude", y="latitude", alpha=0.4,
#     s=housing["population"]/100, label="population", figsize=(10,7),
#     c="median_house_value", cmap=plt.get_cmap("jet"), colorbar=True,
#     sharex=False)
# plt.legend()
# plt.title("housing_prices_scatterplot")
# plt.grid()
# plt.show()



# california_img = mpimg.imread('california.png')
# ax = housing.plot(kind="scatter", x="longitude", y="latitude", figsize=(10,7),
#                        s=housing['population']/100, label="Population",
#                        c="median_house_value", cmap=plt.get_cmap("jet"),
#                        colorbar=False, alpha=0.4,)

# plt.imshow(california_img, extent=[-124.55, -113.80, 32.45, 42.05], alpha=0.5,
#            cmap=plt.get_cmap("jet"))

# plt.ylabel("Latitude", fontsize=14)
# plt.xlabel("Longitude", fontsize=14)

# prices = housing["median_house_value"]
# tick_values = np.linspace(prices.min(), prices.max(), 11)
# cbar = plt.colorbar()
# cbar.ax.set_yticklabels(["$%dk"%(round(v/1000)) for v in tick_values], fontsize=14)
# cbar.set_label('Median House Value', fontsize=16)
# plt.title("california_housing_prices_plot")
# plt.legend(fontsize=16)
# plt.grid()
# plt.savefig("california_housing_prices_plot.png")
# plt.show()



'''
стандартный коэффициент корреляции (standard correlation coefficient),
также называемый коэффициентом корреляции Пирсона (r) (Pearson's r),
между каждой парой атрибутов
'''
# print(housing[housing.columns[:-1]].head())
# corr_matrix = housing[housing.columns[:-1]].corr()

# # насколько каждый атрибут связан со средней стоимостью дома
# print(corr_matrix["median_house_value"].sort_values(ascending=False))


# attributes = ["median_house_value", "median_income", "total_rooms",
#               "housing_median_age"]
# scatter_matrix(housing[attributes], figsize=(12, 8))
# plt.savefig("scatter_matrix_plot.png")
# plt.show()


# housing.plot(kind="scatter", x="median_income", y="median_house_value",
#              alpha=0.1)
# plt.axis([0, 16, 0, 550000])
# plt.title("income_vs_house_value_scatterplot")
# plt.grid()
# plt.show()


# housing = housing[housing.columns[:-1]]

# housing["rooms_per_household"] = housing["total_rooms"]/housing["households"]
# housing["bedrooms_per_room"] = housing["total_bedrooms"]/housing["total_rooms"]
# housing["population_per_household"]=housing["population"]/housing["households"]

# # corr_matrix = housing.corr()
# # print(corr_matrix["median_house_value"].sort_values(ascending=False))


# imputer = SimpleImputer(strategy="median")
# imputer.fit(housing)
# # print(imputer.statistics_)
# # print(housing.median().values)

# X = imputer.transform(housing)
# housing_tr = pd.DataFrame(X, columns=housing.columns, index=housing.index)
# print(housing_tr.head())
# housing_tr.info()
# print(housing_tr.describe())


housing_cat = housing[['ocean_proximity']]
# print(housing_cat.head(25))
ordinal_encoder = OrdinalEncoder()
housing_cat_encoded = ordinal_encoder.fit_transform(housing_cat)
# print(housing_cat_encoded[:25])
print(ordinal_encoder.categories_)