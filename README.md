![](https://gitlab.com/Antoniii/skalern/-/raw/main/meme.jpg)


![](https://gitlab.com/Antoniii/skalern/-/raw/main/housing.png)


![](https://gitlab.com/Antoniii/skalern/-/raw/main/california_housing_prices_plot.png)


![](https://gitlab.com/Antoniii/skalern/-/raw/main/quadratic_predictions_plot.png)


![](https://gitlab.com/Antoniii/skalern/-/raw/main/qubic_predictions_plot.png)


![](https://gitlab.com/Antoniii/skalern/-/raw/main/testfgrtvghsd.JPG)


## Sources

* [ageron/handson-ml3](https://github.com/ageron/handson-ml3)
* [Основы анализа данных на python с использованием pandas+sklearn](https://newtechaudit.ru/metody-pandas-i-scikit-learn-dlya-podgotovki-dannyh/)
* [pip install scikit-learn](https://pypi.org/project/scikit-learn/)
* [Regression](https://scikit-learn.org/stable/modules/svm.html#regression)
* [Загрузка других наборов данных](https://scikit-learn.ru/7-4-loading-other-datasets/)
* [scikit-learn Обучение с учителем 1.1. Линейные модели](https://scikit-learn.ru/category/supervised_learning/)
* [Data Science — 8 главных библиотек для Python программиста](https://python-scripts.com/data-science)
* [unton3ton/oldmemestrends](https://github.com/unton3ton/oldmemestrends)