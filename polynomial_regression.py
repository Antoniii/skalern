import numpy as np
import numpy.random as rnd
import matplotlib.pyplot as plt
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression

np.random.seed(42)

m = 100
# X = 6 * np.random.rand(m, 1) - 3
# y = 0.5 * X**2 + X + 2 + np.random.randn(m, 1)

# plt.plot(X, y, "b.")
# plt.xlabel("$x_1$", fontsize=18)
# plt.ylabel("$y$", rotation=0, fontsize=18)
# plt.axis([-3, 3, 0, 10])
# plt.title('quadratic_data_plot')
# plt.grid()
# plt.savefig("quadratic_data_plot.png")
# plt.show()


# poly_features = PolynomialFeatures(degree=2, include_bias=False)
# X_poly = poly_features.fit_transform(X)
# # print(X[0], X_poly[0])

# lin_reg = LinearRegression()
# lin_reg.fit(X_poly, y)
# print(lin_reg.intercept_, lin_reg.coef_)

# X_new=np.linspace(-3, 3, 100).reshape(100, 1)
# X_new_poly = poly_features.transform(X_new)
# y_new = lin_reg.predict(X_new_poly)
# plt.plot(X, y, "b.")
# plt.plot(X_new, y_new, "r-", linewidth=2, label="Predictions")
# plt.xlabel("$x_1$", fontsize=18)
# plt.ylabel("$y$", rotation=0, fontsize=18)
# plt.legend(loc="upper left", fontsize=14)
# plt.axis([-3, 3, 0, 10])
# plt.title('quadratic_predictions_plot')
# plt.grid()
# plt.savefig("quadratic_predictions_plot.png")
# plt.show()



X = 10 * np.random.rand(m, 1) - 5
y = 0.137 * X**3 + 0.42 * X**2 + X + 1.5 + np.random.randn(m, 1)
# plt.plot(X, y, "b.")
# plt.xlabel("$x_1$", fontsize=18)
# plt.ylabel("$y$", rotation=0, fontsize=18)
# plt.axis([-6, 6, -15, 36])
# plt.title('qubic_data_plot')
# plt.grid()
# plt.savefig("qubic_data_plot.png")
# plt.show()

poly_features = PolynomialFeatures(degree=3, include_bias=False)
X_poly = poly_features.fit_transform(X)
lin_reg = LinearRegression()
lin_reg.fit(X_poly, y)
X_new=np.linspace(-6, 6, 100).reshape(100, 1)
X_new_poly = poly_features.transform(X_new)
y_new = lin_reg.predict(X_new_poly)
# plt.plot(X, y, "b.")
# plt.plot(X_new, y_new, "r-", linewidth=2, label="Predictions")
# plt.xlabel("$x_1$", fontsize=18)
# plt.ylabel("$y$", rotation=0, fontsize=18)
# plt.legend(loc="upper left", fontsize=14)
# plt.axis([-6, 6, -15, 36])
# plt.title('qubic_predictions_plot')
# plt.grid()
# plt.savefig("qubic_predictions_plot.png")
# plt.show()

print(X[0], X_poly[0])
print(X_new_poly[0], y_new[0])